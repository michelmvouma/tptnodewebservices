/**
 * Created by Faliherizo on 24/03/2017.
 */
var express = require('express');
var router = express.Router();
var app = express();
const Scenario = require('../modelsMongo/Scenario');

router
    .get('/',function(req, res) {
        Scenario.find({}).then(function(scenarios){
            if(scenarios==null)
            {
                sendResponseData([], res);
            }else{
                sendResponseData(scenarios, res);
            }
        });

        res.render('scenario', { title: 'Express' });
    });

/**
 *
 * @param data object à renvoyer
 * @param res l'object response de node js
 */
var sendResponseData = function(data,res)
{
    res.send(data, {
        'Content-Type': 'application/json'
    }, 200);
};

module.exports = router;