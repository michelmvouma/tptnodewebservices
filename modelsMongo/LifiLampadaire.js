/**
 * Created by michjobs on 21/03/17.
 */
const Utilisateur = require('./Utilisateur');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var LampadaireSchema = new Schema({
    serialID: String,
    id_user:{type:Schema.ObjectId, ref: 'Utilisateur'},
    dateCreate: Date
}, {collection: 'Lampadaire'});

module.exports = mongoose.model('Lampadaire', LampadaireSchema );