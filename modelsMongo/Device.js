/**
 * Created by group lifi on 21/03/17.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var DeviceSchema = new Schema({
    name:String,  //Nom de devise binding openhab2
    value:String, //Value exemple ( ON, OFF, ... par rapport devise)
    type:String   //checkbox ou Switch, tempmax, tempmin // pas de type encore pour la température
}, {collection: 'Device'});

module.exports = mongoose.model('Device', DeviceSchema);